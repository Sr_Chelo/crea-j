const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const Cliente = require("../models/cliente");
const config = require("../config/database");

module.exports = (passport)=>{
    let opts = {}
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.secret;
    passport.use(new JwtStrategy(opts, (jwt_payload,done)=>{
        Cliente.getClienteById(jwt_payload.username._id, (err,cliente)=>{
            if(err){
                return done(err,false);
            }
            if(cliente){
                return done(null,cliente);
            } else {
                return done(null,false);
            }
        });
    }));
}