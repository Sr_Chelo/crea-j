const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

//Categoria
const CategoriaSchema = mongoose.Schema({
    id:{
        type:Number,
        require: true
    },
    nombre:{
        type:String,
        require:true
    }
})
const Categoria = module.exports = mongoose.model("Categoria",CategoriaSchema);
module.exports.getLastId = (callback)=>{
    Categoria.find().sort({$natural:-1}).limit(1);
}
module.exports.getCategorias = (callback)=>{
    Categoria.find(callback);
}
module.exports.addCat = function(newCategoria,callback){
    this.getLastId((err,callback)=>{
        if(err) throw err;
        newCategoria.id =  callback.id + 1;
        newCategoria.save(callback);
    });
}