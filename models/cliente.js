const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database")

// Client Schema

const ClientSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    DUI: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    empresa: {
        type: String,
        required: true
    },
    contrasena: {
        type: String,
        required: true
    }
});

const Cliente = module.exports = mongoose.model("Cliente",ClientSchema);

module.exports.getClienteById = (id, callback)=>{
    Cliente.findById(id,callback);
}

module.exports.getClienteByIUsername = (username, callback)=>{
    const query = {username:username};
    Cliente.findOne(query,callback);
}
module.exports.getClienteByDUI = (DUI,callback)=>{
    const query = {DUI:DUI};
    Cliente.findOne(query,callback);
}
module.exports.getClienteByEmail = (email,callback)=>{
    const query = {correo:email};
    Cliente.findOne(query,callback);
}
module.exports.addCliente = function(newCliente, callback){
    this.getClienteByIUsername(newCliente.username, (err,call)=>{
        if(call != null){
            callback(true,'Nombre de Usuario ya registrado')
        } else {
            this.getClienteByEmail(newCliente.correo, (err,call)=>{
                if(call != null){
                    callback(true, 'Correo ya registrado')
                } else {
                    this.getClienteByDUI(newCliente.DUI, (err,call)=>{
                        if(call != null){
                            callback(true, 'DUI ya registrado')
                        } else {
                            bcrypt.genSalt(10, (err, salt) => {
                                bcrypt.hash(newCliente.contrasena, salt, (err,hash) => {
                                    if(err) throw err;
                                    newCliente.contrasena = hash;
                                    newCliente.save(callback);
                                })
                            })
                        }
                    })
                }
            })
        }
    })
}
module.exports.comparePassword = function(candidatePassword,hash,callback){
    bcrypt.compare(candidatePassword,hash,(err,isMatch)=>{
        if(err) throw err;
        callback(null, isMatch);
    });
}