const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Producto = require("../models/producto");
const Categoria = require("../models/producto");
const config = require("../config/database");

//Index
router.get('/',passport.authenticate('jwt',{session:false}),(req,res)=>{
    res.json({msg:"Authorized", profile:req.user});
});

//Call Invetario
router.post('/buscar',(req,res)=>{
    Producto.getProducto((err,callback)=>{
        res.json(callback);
    })
});

//Add Producto
router.post('/ingresar',(req,res)=>{
    let newProducto = new Producto({
        nombre :req.body.nombre,
        codigo : req.body.cod,
        categoria : req.body.cat,
        descripcion : req.body.desc,
        existencia : req.body.exis,
        imagen : req.body.pic
    })
    Producto.addProducto(newProducto, (err,callback)=>{
        if(err){
            res.json({err:err});
        } else {
            res.json({msg:"Producto Ingresado"});
        }
    })
});

//Default
router.get('*',(req,res)=>{
    res.send("Invalid");
});
module.exports = router;