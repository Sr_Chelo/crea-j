const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Cliente = require("../models/cliente");
const config = require("../config/database");

//Index
router.get('/perfil',passport.authenticate('jwt',{session:false}),(req,res)=>{
    res.json({msg:"Authorized", profile:req.user});
});
//Registro
router.post('/registro',(req,res)=>{
    let newCliente = new Cliente ({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        username: req.body.username,
        correo: req.body.correo,
        DUI: req.body.DUI,
        telefono: req.body.telefono,
        direccion: req.body.direccion,
        empresa: req.body.empresa,
        contrasena: req.body.contrasena
    });
    Cliente.addCliente(newCliente, (err,call)=>{
        if(err){
            res.json({msg:call});
        } else {
            res.json({msg:"Cliente Registrado"});
        }
    })

});
//Authenticate
router.post('/autentificacion',(req,res)=>{
    const username = req.body.username;
    const contrasena = req.body.contrasena;
    if(!username || !contrasena){
        return res.json({msg:'Datos Incompletos'});
    }
    Cliente.getClienteByIUsername(username, (err,call)=>{
        if(err) throw err;
        if(!call){
            return res.json({msg:"Usuario No Encontrado"});
        }
        Cliente.comparePassword(contrasena,call.contrasena, (err,isMatch)=>{
            if(err) throw err;
            if(isMatch){
                const payload = {cliente:call};
                const token = jwt.sign(payload, config.secret);
                res.json({message: "ok", token:'Bearer '+token, cliente:call});
            } else {
                return res.json({msg:"Contraseña Incorrecta"});
            }
        });
    });
    
});
//Default
router.get('*',(req,res)=>{
    res.send("Invalid");
});
module.exports = router;