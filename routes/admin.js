const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Cliente = require("../models/cliente");
const config = require("../config/database");

router.get('/perfil',passport.authenticate('jwt',{session:false}),(req,res)=>{
    res.json({msg:"Authorized", profile:req.user});
});
//Authenticate
router.post('/autentificacion',(req,res)=>{
    const username = req.body.username;
    const contrasena = req.body.contrasena;
    if(!username || !contrasena){
        return res.json({msg:'Datos Incompletos'});
    }
    const admin ={
        username:'admin',
        contrasena:'madiservi'
    }
    if(username == admin.username && contrasena == admin.contrasena){
        const payload = {admin: admin};
        const token = jwt.sign(payload, config.secret);
        res.json({message: "ok", token:'Bearer '+token, admin});
    } else {
        return res.json({msg:'Usuario Incorrecto'});
    }
    
});
//Ingreso Inventario
router.get('/inventario',(req,res)=>{
    res.send("Inventario");
});

//Default
router.get('*',(req,res)=>{
    res.send("Invalid");
});
module.exports = router;