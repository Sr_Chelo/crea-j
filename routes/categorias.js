const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const Categoria = require("../models/categoria");
const config = require("../config/database");

//Call Cat
router.post('/categoria',(req,res)=>{
    Categoria.getCategorias((err,call)=>{
        if(err) throw err;
        if(!call){
            res.json({msg:"No se pudo completar"});
        } else {
            res.json(call);
        }
    });
})
module.exports = router;