import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class AuthService {
  constructor(private http:HttpClient) {
  }
  registerCliente(cliente){
    return this.http.post('http://localhost:3000/clientes/registro',cliente,httpOptions);
  }
  authenticateCliente(cliente){
    return this.http.post('http://localhost:3000/clientes/autentificacion',cliente,httpOptions);
  }
  callCats(){
    return this.http.post('http://localhost:3000/categorias/categoria',null,httpOptions);
  }
  addProducto(producto){
    return this.http.post('http://localhost:3000/productos/ingresar',producto,httpOptions);
  }
  storeClienteData(data){
    if(data.msg){
      return data.msg;
    } else {
    localStorage.setItem('id_token', data.token);
    localStorage.setItem('id', data.cliente._id);
    localStorage.setItem('nombre',data.cliente.nombre);
    localStorage.setItem('apellido',data.cliente.apellido);
    localStorage.setItem('username',data.cliente.username);
    localStorage.setItem('correo',data.cliente.correo);
    localStorage.setItem('DUI',data.cliente.DUI);
    localStorage.setItem('telefono',data.cliente.telefono);
    localStorage.setItem('direccion',data.cliente.direccion);
    localStorage.setItem('empresa',data.cliente.empresa);
    localStorage.setItem('contrasena',data.cliente.contrasena);
    localStorage.setItem('loged','1');
    httpOptions.headers.set('Authorization',data.token);
    httpOptions.headers.append('Cliente',data.cliente);
    console.log(data);
    }
  }
  obtainValue(data){
    return data.msg;
  }
  obtainValueErr(data){
    return data.err;
  }
  authAdmin(admin){
    return this.http.post('http://localhost:3000/admin/autentificacion',admin,httpOptions);
  }
  callProductos(){
    return this.http.post('http://localhost:3000/productos/buscar',null,httpOptions);
  }
  storeAdmin(data){
    if(data.msg){
      return data.msg;
    } else {
      localStorage.setItem('nombre','admin');
      localStorage.setItem('admin_log','1');
    }
  }
  logout(){
    httpOptions.headers.set('Authorization','my-auth-token');
    httpOptions.headers.delete('Cliente');
    localStorage.clear();
  }
}
