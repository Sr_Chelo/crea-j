import { Component, OnInit, Input } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  nombre = localStorage.getItem('nombre');
  apellido = localStorage.getItem('apellido');
  loged: any = localStorage.getItem('loged');
  adloged:any = localStorage.getItem('admin_log');
  constructor(private AuthService:AuthService, private Router:Router) { }


  ngOnInit() {
    
  }
  onLogout(){
    console.log("Sesion Cerrada");
    this.AuthService.logout();
    location.reload();
  }

}
