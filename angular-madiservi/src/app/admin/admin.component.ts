import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {NgFlashMessageService} from 'ng-flash-messages';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  username:any;
  contrasena:any;
  pic:File = null;
  adloged:any = localStorage.getItem('admin_log');
  loged:any = localStorage.getItem('loged');
  invent:boolean = true;
  categorias:any;

  nombre:string;
  cod:string;
  desc:string;
  cat:number;
  picc:any;
  exis:any;

  constructor(private validateService: ValidateService,
    private AuthService: AuthService,
    private router: Router,
    private ngFlashMessageService: NgFlashMessageService) { }
  addProd(){
    this.invent = false;
  }
  onSubmit(){
    this.invent = true;
  }
  onChangePic(event){
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      if(file.size > 60000){
        alert("La Imagen es demasiado grande");
        return false;
      }
      
      reader.onloadend = ()=>{
        this.picc = reader.result;
      }
      if(file){
        reader.readAsDataURL(file);
      }
    }
  }
  onSubmitAdd(){
    const producto = {
      nombre: this.nombre,
      cod: this.cod,
      desc:this.desc,
      pic:this.picc,
      cat:this.cat,
      exis:'1'
    }
    this.AuthService.addProducto(producto).subscribe(data=>{
      const msg = this.AuthService.obtainValue(data);
      const err = this.AuthService.obtainValueErr(data);
      if(msg){
        this.ngFlashMessageService.showFlashMessage({
          messages: [msg],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
      }else{
        this.ngFlashMessageService.showFlashMessage({
          messages: [err],
          dismissible: true,
          timeout: false,
          type: 'danger'
        });
      }
    }, error =>{console.log("Error ",error);
    });
  }
  onSubmitAd(){
    const admin ={
      username: this.username,
      contrasena: this.contrasena
    }
    this.AuthService.authAdmin(admin).subscribe(data=>{
      const msg = this.AuthService.obtainValue(data);
      if(this.AuthService.storeAdmin(data)){
        this.ngFlashMessageService.showFlashMessage({
          messages: [msg],
          dismissible: true,
          timeout: false,
          type: 'danger'
        });
      }else{
        this.ngFlashMessageService.showFlashMessage({
          messages: ['Sesion Iniciada'],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
        location.reload();
      }
    },err=>{
      console.log(err);
    })
  }
  ngOnInit() {
    if(this.loged == '1'){
      this.router.navigate(['/']);
    }
    this.AuthService.callCats().subscribe(data=>{
      this.categorias = data;
    })
  }

}
