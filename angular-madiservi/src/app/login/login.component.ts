import { Component, OnInit, EventEmitter,Output } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {NgFlashMessageService} from 'ng-flash-messages';
import { ResourceLoader } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  contrasena: String;
  loged:any = localStorage.getItem('loged');

  constructor(private AuthService:AuthService, private Router:Router,private ngFlashMessageService: NgFlashMessageService) { }
  ngOnInit() {
    if(this.loged == '1'){
      this.Router.navigate(['/']);
    }
  }
  onLoginSubmit(){
    const cliente = {
      username: this.username,
      contrasena: this.contrasena
    }
    this.AuthService.authenticateCliente(cliente).subscribe(data=>{
      const msg = this.AuthService.obtainValue(data);
      if(this.AuthService.storeClienteData(data)){
        this.ngFlashMessageService.showFlashMessage({
          messages: [msg],
          dismissible: true,
          timeout: false,
          type: 'danger'
        });
      } else {
      this.ngFlashMessageService.showFlashMessage({
        messages: ['Sesion Iniciada'],
        dismissible: true,
        timeout: false,
        type: 'success'
      });
      location.reload();
    }
    }, error =>{
      
    });
  }
  alerta(data){
    alert(data);
  }

}
