import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../services/validate.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {NgFlashMessageService} from 'ng-flash-messages';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  nombre: String;
  apellido: String;
  username: String;
  correo: String;
  DUI: String;
  telefono: String;
  direccion: String;
  empresa: String;
  contrasena:String;
  loged:any = localStorage.getItem('loged');

  constructor(private validateService: ValidateService,
    private AuthService: AuthService,
    private router: Router,
    private ngFlashMessageService: NgFlashMessageService) { }

  ngOnInit() {
    if(this.loged == '1'){
      this.router.navigate(['/']);
    }
  }
  onRegisterSubmit(){
    const cliente = {
      nombre: this.nombre,
      apellido: this.apellido,
      username: this.username,
      correo: this.correo,
      DUI: this.DUI,
      telefono: this.telefono,
      direccion: this.direccion,
      empresa: this.empresa,
      contrasena: this.contrasena
    }
    // Required fields
    if(!this.validateService.validateRegister(cliente)){
      this.ngFlashMessageService.showFlashMessage({
        messages: ['Por Favor, llena todos los campos de texto'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }

    //validate Email
    if(!this.validateService.validateEmail(cliente.correo)){
      this.ngFlashMessageService.showFlashMessage({
        messages: ['Por Favor, usa un correo valido'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }
    if(!this.validateService.validateDUI(cliente.DUI)){
      this.ngFlashMessageService.showFlashMessage({
        messages: ['Por Favor, usa un DUI valido'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }
    if(!this.validateService.validateTelefono(cliente.telefono)){
      this.ngFlashMessageService.showFlashMessage({
        messages: ['Por Favor, usa un teléfono valido'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }
    this.AuthService.registerCliente(cliente).subscribe(data=>{
      this.alerta(JSON.stringify(data));
      this.router.navigate(['/login']);
    }, error =>{console.log("Error ",error);
    });


  }
  alerta(data){
    alert(data.toString());
  }

}
