const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const config = require("./config/database.js");

//Connect to database
mongoose.connect(config.uris,config.options);
mongoose.connection.on('connected', () => {
    console.log('Connected to database'+config.uris);
})
mongoose.connection.on('error', (err) => {
    console.log('Database error'+err);
})

const app = express();

const users = require('./routes/clientes');
const admin = require('./routes/admin');
const productos = require('./routes/productos');
const categorias = require('./routes/categorias');

//port Number
const port = 3000;

//CORS Middleware
app.use(cors());

//Set static Folfer
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/clientes',users);
app.use('/admin',admin);
app.use('/productos',productos);
app.use('/categorias',categorias);


//Index Route
app.get('/', (req,res)=> {
    res.send("Invalid Endpoint");
});

//Start Server
app.listen(port, () => {
    console.log("Server started on port "+port);
});